<?php

namespace Strictly\Log;

interface Writer
{
    /**
     * @param string $message
     */
    function debug(string $message);
}