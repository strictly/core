<?php

namespace Strictly\Log;

use Carbon\Carbon;
use Strictly\Filesystem\Repository;

class FileWriter implements Writer
{
    /**
     * @var resource
     */
    private $file;

    /**
     * FileWriter constructor.
     * @param Repository $paths
     */
    public function __construct(Repository $paths)
    {
        $this->file = fopen($paths->getLogPath().DIRECTORY_SEPARATOR.'app.log', 'a');
    }

    /**
     * @param string $message
     */
    function debug(string $message)
    {
        fwrite($this->file, "[".Carbon::now()->toDateTimeString()."] DEBUG: ".$message.PHP_EOL);
    }
}