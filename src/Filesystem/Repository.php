<?php

namespace Strictly\Filesystem;

interface Repository
{
    /**
     * @return string
     */
    function getBasePath(): string;

    /**
     * @return string
     */
    function getConfigPath(): string;

    /**
     * @return string
     */
    function getPublicPath(): string;

    /**
     * @return string
     */
    function getLogPath(): string;

    /**
     * @return string
     */
    function getRoutesPath(): string;
}