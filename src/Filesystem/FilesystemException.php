<?php

namespace Strictly\Filesystem;

use LogicException;

class FilesystemException extends LogicException {}