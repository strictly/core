<?php

namespace Strictly\Filesystem;

class BasePathRelativeRepository implements Repository
{
    /**
     * @var string
     */
    private $basePath;

    /**
     * ArrayRepository constructor.
     * @param string $basePath
     */
    public function __construct(string $basePath)
    {
        $this->basePath = $basePath;
    }

    /**
     * @return string
     */
    function getBasePath(): string
    {
        return $this->basePath;
    }

    /**
     * @return string
     */
    function getConfigPath(): string
    {
        return $this->basePath.DIRECTORY_SEPARATOR.'config';
    }

    /**
     * @return string
     */
    function getPublicPath(): string
    {
        return $this->basePath.DIRECTORY_SEPARATOR.'public';
    }

    /**
     * @return string
     */
    function getLogPath(): string
    {
        return $this->basePath.DIRECTORY_SEPARATOR.'logs';
    }

    /**
     * @return string
     */
    function getRoutesPath(): string
    {
        return $this->basePath.DIRECTORY_SEPARATOR.'routes';
    }
}