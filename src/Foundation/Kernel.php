<?php

namespace Strictly\Foundation;

use Strictly\Configuration\Loader;
use Strictly\Container\ResolverInterface;

class Kernel implements \Strictly\Foundation\Contracts\KernelInterface
{
    /**
     * @var ServiceProviderLoaderInterface
     */
    private $serviceProviderLoader;

    /**
     * @var Loader
     */
    private $configurationLoader;

    /**
     * @var ResolverInterface
     */
    private $container;

    /**
     * Application constructor.
     * @param ServiceProviderLoaderInterface $serviceProviderLoader
     * @param Loader $configurationLoader
     * @param ResolverInterface $container
     */
    public function __construct(
        ServiceProviderLoaderInterface $serviceProviderLoader,
        Loader $configurationLoader,
        ResolverInterface $container
    ) {
        $this->serviceProviderLoader = $serviceProviderLoader;
        $this->configurationLoader = $configurationLoader;
        $this->container = $container;
    }

    /**
     * Boots the application.
     */
    function boot()
    {
        $this->configurationLoader->load();
        $this->serviceProviderLoader->register();
        $this->serviceProviderLoader->configure();
    }

    /**
     * Shuts down the application.
     */
    function terminate()
    {
        // TODO: Implement terminate() method.
    }
}