<?php

namespace Strictly\Foundation;

use RuntimeException;
use Strictly\Configuration\Repository;
use Strictly\Container\ResolverInterface;
use Strictly\Foundation\Contracts\ServiceProvider;

class ServiceProviderLoader implements ServiceProviderLoaderInterface
{
    /**
     * @var Repository
     */
    private $config;

    /**
     * @var ResolverInterface
     */
    private $container;

    /**
     * @var \Strictly\Container\Repository
     */
    private $bindings;

    /**
     * ServiceProviderLoader constructor.
     * @param Repository $config
     * @param ResolverInterface $container
     * @param \Strictly\Container\Repository $bindings
     */
    public function __construct(Repository $config, ResolverInterface $container, \Strictly\Container\Repository $bindings)
    {
        $this->config = $config;
        $this->container = $container;
        $this->bindings = $bindings;
    }

    /**
     * Register the application services.
     */
    function register()
    {
        /** @var array|string[] $serviceProviderReferences */
        if(!$serviceProviderReferences = $this->config->get('providers')) {
            throw new RuntimeException("Failed to load service providers.");
        }

        foreach($serviceProviderReferences as $serviceProviderReference) {
            /** @var ServiceProvider $serviceProvider */
            $serviceProvider = $this->container->resolve($serviceProviderReference);

            $serviceProvider->register();
        }
    }

    /**
     * Configure the application services.
     */
    function configure()
    {
        /** @var array|string[] $serviceProviderReferences */
        if(!$serviceProviderReferences = $this->config->get('providers')) {
            throw new RuntimeException("Failed to load service providers.");
        }

        foreach($serviceProviderReferences as $serviceProviderReference) {
            /** @var ServiceProvider $serviceProvider */
            $serviceProvider = $this->container->resolve($serviceProviderReference);

            $serviceProvider->configure();
        }
    }
}