<?php

namespace Strictly\Foundation\Contracts;

interface ServiceProvider
{
    /**
     * Register Bindings.
     */
    function register();

    /**
     * Configure system.
     */
    function configure();
}