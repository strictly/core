<?php

namespace Strictly\Foundation\Contracts;

interface KernelInterface
{
    /**
     * Boots the application.
     */
    function boot();

    /**
     * Shuts down the application.
     */
    function terminate();
}