<?php

namespace Strictly\Foundation\Contracts;

interface Application
{
    /**
     * Boots the application.
     */
    function boot();
}