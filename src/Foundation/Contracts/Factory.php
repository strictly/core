<?php

namespace Strictly\Foundation\Contracts;

interface Factory
{
    /**
     * @return mixed
     */
    function make();
}