<?php

namespace Strictly\Foundation;

interface ServiceProviderLoaderInterface
{
    /**
     * Register the application services.
     */
    function register();

    /**
     * Configure the application services.
     */
    function configure();
}