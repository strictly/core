<?php

namespace Strictly\Console;

use Strictly\Command\Command;

interface ConsoleCommand extends Command
{
    /**
     * @return string
     */
    function getName(): string;

    /**
     * @return string
     */
    function getDescription(): string;
}