<?php

namespace Strictly\Console;

use Exception;
use Throwable;

class CommandException extends Exception
{
    /**
     * @var ConsoleCommand $command
     */
    private $command;

    /**
     * CommandException constructor.
     * @param ConsoleCommand $commandName
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(ConsoleCommand $commandName, string $message = "", int $code = 0, Throwable $previous = null)
    {
        $this->command = $commandName;

        parent::__construct($message, $code, $previous);
    }

    /**
     * @return ConsoleCommand
     */
    function getCommand(): ConsoleCommand {
        return $this->command;
    }
}