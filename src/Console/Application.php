<?php

namespace Strictly\Console;

use Strictly\Console\Commands\CommandNotFoundException;
use Strictly\Console\Commands\Repository;
use Strictly\Container\ResolverInterface;
use Strictly\Foundation\Contracts\KernelInterface;
use Throwable;

class Application implements ApplicationInterface
{
    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @var ResolverInterface
     */
    private $container;

    /**
     * Application constructor.
     * @param KernelInterface $kernel
     * @param ResolverInterface $container
     */
    public function __construct(
        KernelInterface $kernel,
        ResolverInterface $container
    ) {
        $this->kernel = $kernel;
        $this->container = $container;
    }

    /**
     * Execute the command.
     */
    function handle()
    {
        $this->kernel->boot();

        /** @var ExceptionHandler $exceptionHandler */
        $exceptionHandler = $this->container->resolve(ExceptionHandler::class);

        try {
            /** @var Repository $commandRepository */
            $commandRepository = $this->container->resolve(Repository::class);

            // TODO: Get arguments from injected class.
            global $argv;
            if(!$commandName = $argv[1] ?? null) {
                throw new CommandNotFoundException("No command.");
            }

            $commandRepository->get($commandName)->execute();
        } catch (Throwable $throwable) {
            $exceptionHandler->handle($throwable);
        }

        $this->kernel->terminate();
    }
}