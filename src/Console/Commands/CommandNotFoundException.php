<?php

namespace Strictly\Console\Commands;

use RuntimeException;
use Throwable;

class CommandNotFoundException extends RuntimeException {

    /**
     * @var string
     */
    private $commandName;

    /**
     * CommandNotFoundException constructor.
     * @param string $message
     * @param string $commandName
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "", string $commandName = null, int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->commandName = $commandName;
    }

    /**
     * @return string|null
     */
    function commandName() {
        return $this->commandName;
    }
}