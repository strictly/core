<?php

namespace Strictly\Console\Commands;

use Strictly\Console\ConsoleCommand;

class ArrayRepository implements Repository
{
    /**
     * @var ConsoleCommand[]
     */
    private $commands = [];

    /**
     * @param string $name
     * @param ConsoleCommand $consoleCommand
     */
    function set(string $name, ConsoleCommand $consoleCommand)
    {
        $this->commands[$name] = $consoleCommand;
    }

    /**
     * @param string $name
     * @return ConsoleCommand
     */
    function get(string $name): ConsoleCommand
    {
        $consoleCommand = $this->commands[$name] ?? null;

        if(!$consoleCommand) {
            throw new CommandNotFoundException("Command '$name' does not exist.", $name);
        }

        return $consoleCommand;
    }

    /**
     * @return ConsoleCommand[]
     */
    function all(): array
    {
        return $this->commands;
    }
}