<?php

namespace Strictly\Console\Commands;

use Strictly\Console\ConsoleCommand;

interface Repository
{
    /**
     * @param string $name
     * @param ConsoleCommand $consoleCommand
     */
    function set(string $name, ConsoleCommand $consoleCommand);

    /**
     * @param string $name
     * @return ConsoleCommand
     */
    function get(string $name): ConsoleCommand;

    /**
     * @return ConsoleCommand[]
     */
    function all(): array;
}