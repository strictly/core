<?php

namespace Strictly\Console\Commands;

use Strictly\Console\ConsoleCommand;

class ListCommands implements ConsoleCommand
{
    /**
     * @var Repository
     */
    private $commandRepository;

    /**
     * ListCommands constructor.
     * @param Repository $commandRepository
     */
    public function __construct(Repository $commandRepository)
    {
        $this->commandRepository = $commandRepository;
    }

    /**
     * Execute the command.
     */
    function execute()
    {
        print "Commands:".PHP_EOL;

        foreach($this->commandRepository->all() as $consoleCommand) {
            print "{$consoleCommand->getName()}: {$consoleCommand->getDescription()}".PHP_EOL;
        }
    }

    /**
     * @return string
     */
    function getName(): string
    {
        return 'list';
    }

    /**
     * @return string
     */
    function getDescription(): string
    {
        return 'List all of the available commands.';
    }
}