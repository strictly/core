<?php

namespace Strictly\Console\Commands;

use Strictly\Command\Command;
use Strictly\Console\ConsoleCommand;
use Strictly\Container\ResolverInterface;

class CommandLoader implements Command
{
    /**
     * @var \Strictly\Configuration\Repository
     */
    private $configurationRepository;

    /**
     * @var Repository
     */
    private $commandRepository;

    /**
     * @var ResolverInterface
     */
    private $container;

    /**
     * CommandLoader constructor.
     * @param \Strictly\Configuration\Repository $configurationRepository
     * @param Repository $commandRepository
     * @param ResolverInterface $container
     */
    public function __construct(
        \Strictly\Configuration\Repository $configurationRepository,
        Repository $commandRepository,
        ResolverInterface $container
    ) {
        $this->configurationRepository = $configurationRepository;
        $this->commandRepository = $commandRepository;
        $this->container = $container;
    }

    /**
     * Execute the command.
     */
    function execute()
    {
        /** @var string[] $commandReferences */
        $commandReferences = $this->configurationRepository->get('commands') ?? [];

        foreach($commandReferences as $commandReference) {
            /** @var ConsoleCommand $command */
            $command = $this->container->resolve($commandReference);
            $this->commandRepository->set($command->getName(), $command);
        }
    }
}