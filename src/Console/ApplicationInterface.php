<?php

namespace Strictly\Console;

interface ApplicationInterface
{
    /**
     * Handle command.
     */
    function handle();
}