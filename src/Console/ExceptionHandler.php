<?php

namespace Strictly\Console;

use Strictly\Console\Commands\CommandNotFoundException;
use Throwable;

interface ExceptionHandler
{
    /**
     * @param Throwable $throwable
     */
    function handle(Throwable $throwable);

    /**
     * @param CommandException $exception
     */
    function handleCommandException(CommandException $exception);

    /**
     * @param CommandNotFoundException $exception
     */
    function handleCommandNotFoundException(CommandNotFoundException $exception);
}