<?php

namespace Strictly\Configuration;

use Symfony\Component\Finder\Finder;

class FileLoader implements Loader
{
    /**
     * @var Repository
     */
    private $configurationRepository;

    /**
     * @var Finder
     */
    private $finder;

    /**
     * @var \Strictly\Filesystem\Repository
     */
    private $filesystemRepository;

    /**
     * FileLoader constructor.
     * @param \Strictly\Filesystem\Repository $filesystemRepository
     * @param Repository $configurationRepository
     * @param Finder $finder
     */
    public function __construct(\Strictly\Filesystem\Repository $filesystemRepository, Repository $configurationRepository, Finder $finder)
    {
        $this->filesystemRepository = $filesystemRepository;
        $this->configurationRepository = $configurationRepository;
        $this->finder = $finder;
    }

    /**
     * Load the configurations.
     */
    function load()
    {
        foreach($this->finder->in($this->filesystemRepository->getConfigPath())->name('*.php')->files()->getIterator() as $splFileInfo) {
            $key = explode('.', $splFileInfo->getFilename())[0];
            $data = require_once $splFileInfo->getRealPath();
            $this->configurationRepository->set($key, $data);
        }
    }
}