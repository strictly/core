<?php

namespace Strictly\Configuration;

interface Repository
{
    /**
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    function get(string $key, $default = null);

    /**
     * @param string $key
     * @param mixed $value
     */
    function set(string $key, $value);
}