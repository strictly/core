<?php

namespace Strictly\Configuration\DotAdapter;

use Adbar\Dot;
use Strictly\Foundation\Contracts\Factory;

class SimpleDotAdapterFactory implements Factory
{
    /**
     * @return mixed
     */
    function make()
    {
        return new DotAdapter(new Dot());
    }
}