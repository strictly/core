<?php

namespace Strictly\Configuration\DotAdapter;

use Adbar\Dot;
use Strictly\Configuration\Repository;

class DotAdapter implements Repository
{
    /**
     * @var Dot
     */
    private $configuration;

    /**
     * EnvironmentVariableRepository constructor.
     * @param Dot $configuration
     */
    public function __construct(Dot $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    function get(string $key, $default = null)
    {
        return $this->configuration->get($key, $default);
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    function set(string $key, $value)
    {
        return $this->configuration->set($key, $value);
    }
}