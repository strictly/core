<?php

namespace Strictly\Configuration\DotEnv;

use Dotenv\Dotenv;
use Strictly\Configuration\Loader;

class DotEnvLoader implements Loader
{
    /**
     * @var Dotenv
     */
    private $dotEnv;

    /**
     * DotEnvLoader constructor.
     * @param Dotenv $dotEnv
     */
    public function __construct(Dotenv $dotEnv)
    {
        $this->dotEnv = $dotEnv;
    }

    /**
     * Load the configurations.
     */
    function load()
    {
        $this->dotEnv->load();
    }
}