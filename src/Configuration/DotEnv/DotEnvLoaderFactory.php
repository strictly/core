<?php

namespace Strictly\Configuration\DotEnv;

use Strictly\Filesystem\Repository;
use Strictly\Foundation\Contracts\Factory;

class DotEnvLoaderFactory implements Factory
{
    /**
     * @var Repository
     */
    private $paths;

    /**
     * DotEnvLoaderFactory constructor.
     * @param Repository $paths
     */
    public function __construct(Repository $paths)
    {
        $this->paths = $paths;
    }

    /**
     * @return mixed
     */
    function make()
    {
        return new DotEnvLoader(new \Dotenv\Dotenv($this->paths->getBasePath()));
    }
}