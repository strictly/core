<?php

namespace Strictly\Configuration;

interface Loader
{
    /**
     * Load the configurations.
     */
    function load();
}