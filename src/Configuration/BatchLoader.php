<?php

namespace Strictly\Configuration;

class BatchLoader implements Loader
{
    /**
     * @var Loader[]
     */
    private $loaders = [];

    /**
     * @param Loader $loader
     * @return BatchLoader
     */
    function addLoader(Loader $loader) {
        $this->loaders[] = $loader;

        return $this;
    }

    /**
     * Load the configurations.
     */
    function load()
    {
        foreach($this->loaders as $loader) {
            $loader->load();
        }
    }
}