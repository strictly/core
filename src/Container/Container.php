<?php

namespace Strictly\Container;

interface Container extends Repository, ResolverInterface {}