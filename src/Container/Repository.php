<?php

namespace Strictly\Container;

use Closure;

interface Repository
{
    /**
     * @param string $reference
     * @param mixed $instance
     */
    function setInstance(string $reference, $instance);

    /**
     * @param string $reference
     * @param Closure $closure
     * @param bool $singleton
     */
    function setClosure(string $reference, Closure $closure, bool $singleton = false);

    /**
     * @param string $referenceFrom
     * @param string $referenceTo
     * @param bool $singleton
     */
    function setReference(string $referenceFrom, string $referenceTo, bool $singleton = false);

    /**
     * @param string $reference
     * @param string $factory
     * @param bool $singleton
     */
    function setFactory(string $reference, string $factory, bool $singleton = false);

    /**
     * @param string $reference
     * @return mixed
     */
    function getInstance(string $reference);

    /**
     * @param string $reference
     * @return Binding|null
     */
    function getReference(string $reference);

    /**
     * @param string $reference
     * @return Binding|null
     */
    function getClosure(string $reference);

    /**
     * @param string $reference
     * @return Binding|null
     */
    function getFactory(string $reference);
}