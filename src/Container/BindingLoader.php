<?php

namespace Strictly\Container;

interface BindingLoader
{
    /**
     * Load the core application bindings.
     */
    function loadBindings();
}