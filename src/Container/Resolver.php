<?php

namespace Strictly\Container;

use Closure;
use InvalidArgumentException;
use ReflectionClass;
use ReflectionException;
use ReflectionFunction;
use ReflectionParameter;
use Strictly\Foundation\Contracts\Factory;

class Resolver implements ResolverInterface
{
    /**
     * @var Repository
     */
    private $bindingRepository;

    /**
     * Resolver constructor.
     * @param Repository $bindingRepository
     */
    function __construct(Repository $bindingRepository)
    {
        $this->bindingRepository = $bindingRepository;
    }

    /**
     * @param string $reference
     * @return mixed
     */
    function resolve(string $reference) {
        if(!is_null($instanceBinding = $this->bindingRepository->getInstance($reference))) {
            return $instanceBinding;
        }

        // Resolve the closure
        $closureBinding = $this->bindingRepository->getClosure($reference);
        if(!(is_null($closureBinding)) && $closureBinding->getBinding() instanceof Closure) {
            $closureResult = $this->resolveClosure($closureBinding->getBinding());

            if($closureBinding->isSingleton()) {
                $this->bindingRepository->setInstance($reference, $closureResult);
            }

            return $closureResult;
        }

        // Resolve a reference
        if(!is_null($referenceToBinding = $this->bindingRepository->getReference($reference))) {
            $referenceResult = $this->resolve($referenceToBinding->getBinding());

            if($referenceToBinding->isSingleton()) {
                $this->bindingRepository->setInstance($reference, $referenceResult);
            }

            return $referenceResult;
        }

        // Resolve using factory
        if(!is_null($factoryBinding = $this->bindingRepository->getFactory($reference))) {
            /** @var Factory $factory */
            $factory = $this->resolveClass($factoryBinding->getBinding());

            $instance = $factory->make();

            if($factoryBinding->isSingleton()) {
                $this->bindingRepository->setInstance($reference, $instance);
            }

            return $instance;
        }

        // Attempt to resolve unbound references
        return $this->resolveClass($reference);
    }

    /**
     * @param Closure $closure
     * @return mixed
     */
    function resolveClosure(Closure $closure) {
        try {
            $reflectionClosure = new ReflectionFunction($closure);
        } catch (ReflectionException $e) {
            throw new InvalidArgumentException("Function does not exist.");
        }

        // Call closure
        if($reflectionClosure->getNumberOfParameters() === 0) {
            return $closure();
        }

        $parameters = [];

        // Resolve parameters
        foreach($reflectionClosure->getParameters() as $reflectionParameter) {
            $parameters[] = $this->resolveReflectionParameter($reflectionParameter);
        }

        // Call closure with resolved parameters
        return call_user_func_array($closure, $parameters);
    }

    /**
     * @param string $reference
     * @return mixed
     */
    function resolveClass(string $reference) {
        try {
            $reflectionClass = new ReflectionClass($reference);
        } catch (ReflectionException $e) {
            throw new InvalidArgumentException("Class does not exist '$reference'.");
        }

        if(!$reflectionClass->isInstantiable()) {
            throw new BindingResolutionException("Unable to resolve dependency '$reference'.");
        }

        // Create a new instance without calling constructor
        if(is_null($reflectionClass->getConstructor())) {
            return $reflectionClass->newInstanceWithoutConstructor();
        }

        // Create a new instance with no parameters
        if($reflectionClass->getConstructor()->getNumberOfParameters() === 0) {
            return $reflectionClass->newInstance();
        }

        $reflectionParameters = [];

        foreach($reflectionClass->getConstructor()->getParameters() as $reflectionParameter) {
            $reflectionParameters[] = $this->resolveReflectionParameter($reflectionParameter);
        }

        return $reflectionClass->newInstanceArgs($reflectionParameters);
    }

    /**
     * @param ReflectionParameter $reflectionParameter
     * @return mixed
     */
    private function resolveReflectionParameter(ReflectionParameter $reflectionParameter) {
        // Resolve parameters with no type
        if(!$reflectionParameter->hasType() || !$reflectionParameter->getClass()) {
            if(!$reflectionParameter->isDefaultValueAvailable()) {
                throw new BindingResolutionException("Unable to resolve dependency '{$reflectionParameter->getName()}'.");
            } else {
                return $reflectionParameter->getDefaultValue();
            }
        }

        // Resolve Class
        return $this->resolve($reflectionParameter->getClass()->getName());
    }
}