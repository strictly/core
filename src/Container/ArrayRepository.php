<?php

namespace Strictly\Container;

use Closure;

class ArrayRepository implements Repository
{
    /**
     * @var array
     */
    private $instances = [];

    /**
     * @var Binding[]
     */
    private $closures = [];

    /**
     * @var Binding[]
     */
    private $references = [];

    /**
     * @var Binding[]
     */
    private $factories = [];

    /**
     * @param string $reference
     * @param mixed $instance
     */
    function setInstance(string $reference, $instance)
    {
        $this->instances[$reference] = $instance;
    }

    /**
     * @param string $reference
     * @param Closure $closure
     * @param bool $singleton
     */
    function setClosure(string $reference, Closure $closure, bool $singleton = false)
    {
        $this->closures[$reference] = new Binding($closure, $singleton);
    }

    /**
     * @param string $referenceFrom
     * @param string $referenceTo
     * @param bool $singleton
     */
    function setReference(string $referenceFrom, string $referenceTo, bool $singleton = false)
    {
        $this->references[$referenceFrom] = new Binding($referenceTo, $singleton);
    }

    /**
     * @param string $reference
     * @param string $factory
     * @param bool $singleton
     */
    function setFactory(string $reference, string $factory, bool $singleton = false)
    {
        $this->factories[$reference] = new Binding($factory, $singleton);
    }

    /**
     * @param string $reference
     * @return mixed
     */
    function getInstance(string $reference)
    {
        return $this->instances[$reference] ?? null;
    }

    /**
     * @param string $reference
     * @return Binding
     */
    function getReference(string $reference)
    {
        return $this->references[$reference] ?? null;
    }

    /**
     * @param string $reference
     * @return Binding
     */
    function getClosure(string $reference)
    {
        return $this->closures[$reference] ?? null;
    }

    /**
     * @param string $reference
     * @return Binding|null
     */
    function getFactory(string $reference)
    {
        return $this->factories[$reference] ?? null;
    }
}