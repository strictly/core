<?php

namespace Strictly\Container;

class Binding
{
    /**
     * @var mixed
     */
    private $binding;

    /**
     * @var bool
     */
    private $singleton;

    /**
     * Binding constructor.
     * @param $binding
     * @param bool $singleton
     */
    public function __construct($binding, bool $singleton)
    {
        $this->binding = $binding;
        $this->singleton = $singleton;
    }

    /**
     * @return mixed
     */
    function getBinding() {
        return $this->binding;
    }

    /**
     * @return bool
     */
    function isSingleton() {
        return $this->singleton;
    }
}