<?php

namespace Strictly\Container;

use Closure;

class PassthroughFacade implements Container
{
    /**
     * @var Repository
     */
    private $repository;

    /**
     * @var ResolverInterface
     */
    private $resolver;

    /**
     * Facade constructor.
     * @param Repository $repository
     * @param ResolverInterface $resolver
     */
    public function __construct(Repository $repository, ResolverInterface $resolver)
    {
        $this->repository = $repository;
        $this->resolver = $resolver;
    }

    /**
     * @param string $reference
     * @param mixed $instance
     */
    function setInstance(string $reference, $instance)
    {
        $this->repository->setInstance($reference, $instance);
    }

    /**
     * @param string $reference
     * @param Closure $closure
     * @param bool $singleton
     */
    function setClosure(string $reference, Closure $closure, bool $singleton = false)
    {
        $this->repository->setClosure($reference, $closure, $singleton);
    }

    /**
     * @param string $referenceFrom
     * @param string $referenceTo
     * @param bool $singleton
     */
    function setReference(string $referenceFrom, string $referenceTo, bool $singleton = false)
    {
        $this->repository->setReference($referenceFrom, $referenceTo, $singleton);
    }

    /**
     * @param string $reference
     * @param string $factory
     * @param bool $singleton
     */
    function setFactory(string $reference, string $factory, bool $singleton = false)
    {
        $this->repository->setFactory($reference, $factory, $singleton);
    }

    /**
     * @param string $reference
     * @return mixed
     */
    function getInstance(string $reference)
    {
        return $this->repository->getInstance($reference);
    }

    /**
     * @param string $reference
     * @return Binding|null
     */
    function getReference(string $reference)
    {
        return $this->repository->getReference($reference);
    }

    /**
     * @param string $reference
     * @return Binding|null
     */
    function getClosure(string $reference)
    {
        return $this->repository->getClosure($reference);
    }

    /**
     * @param string $reference
     * @return Binding|null
     */
    function getFactory(string $reference)
    {
        return $this->repository->getFactory($reference);
    }

    /**
     * @param string $reference
     * @return mixed
     */
    function resolve(string $reference)
    {
        return $this->resolver->resolve($reference);
    }

    /**
     * @param Closure $closure
     * @return mixed
     */
    function resolveClosure(Closure $closure)
    {
        return $this->resolver->resolveClosure($closure);
    }

    /**
     * @param string $reference
     * @return mixed
     */
    function resolveClass(string $reference)
    {
        return $this->resolver->resolveClass($reference);
    }
}