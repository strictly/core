<?php

namespace Strictly\Container;

use LogicException;

class FileBindingLoader implements BindingLoader
{
    /**
     * @var \Strictly\Configuration\Repository
     */
    private $configurationRepository;

    /**
     * @var Repository
     */
    private $bindingRepository;

    /**
     * FileBindingLoader constructor.
     * @param \Strictly\Configuration\Repository $configurationRepository
     * @param Repository $bindingRepository
     */
    public function __construct(\Strictly\Configuration\Repository $configurationRepository, Repository $bindingRepository)
    {
        $this->configurationRepository = $configurationRepository;
        $this->bindingRepository = $bindingRepository;
    }

    /**
     * Load the core application bindings.
     */
    function loadBindings()
    {
        if(!$bindings = $this->configurationRepository->get('bootstrap.bindings')) {
            throw new LogicException("Unable to load application bindings.");
        }

        foreach($bindings as $from => $to) {
            $this->bindingRepository->setReference($from, $to);
        }
    }
}