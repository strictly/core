<?php

namespace Strictly\Container;

use LogicException;

class BindingException extends LogicException {}