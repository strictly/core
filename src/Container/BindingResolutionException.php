<?php

namespace Strictly\Container;

use LogicException;

class BindingResolutionException extends LogicException {}