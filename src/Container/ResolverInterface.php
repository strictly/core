<?php

namespace Strictly\Container;

use Closure;

interface ResolverInterface
{
    /**
     * @param string $reference
     * @return mixed
     */
    function resolve(string $reference);

    /**
     * @param Closure $closure
     * @return mixed
     */
    function resolveClosure(Closure $closure);

    /**
     * @param string $reference
     * @return mixed
     */
    function resolveClass(string $reference);
}