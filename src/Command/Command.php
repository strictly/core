<?php

namespace Strictly\Command;

interface Command
{
    /**
     * Execute the command.
     */
    function execute();
}