<?php

namespace Http;

use Strictly\Command\Command;
use Strictly\Http\Middleware\Middleware;
use Strictly\Request\RequestInterface;

class HandleRequest implements Command
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var array|\Strictly\Http\Middleware\Middleware[]
     */
    private $middlewares;

    /**
     * HandleRequest constructor.
     * @param RequestInterface $request
     * @param array|\Strictly\Http\Middleware\Middleware[] $middlewares
     */
    public function __construct(RequestInterface $request, array $middlewares)
    {
        $this->request = $request;
        $this->middlewares = $middlewares;
    }

    function execute()
    {
        foreach($this->middlewares as $middleware) {
            $middleware->handle($this->request);
        }
    }
}