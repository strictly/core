<?php

namespace Strictly\Http;

use Strictly\Http\Response\ResponseInterface;

interface ApplicationInterface
{
    /**
     * @return ResponseInterface
     */
    function handle(): ResponseInterface;
}