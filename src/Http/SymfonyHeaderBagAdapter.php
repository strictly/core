<?php

namespace Strictly\Http;

class SymfonyHeaderBagAdapter implements HeaderBag
{
    /**
     * @var \Symfony\Component\HttpFoundation\HeaderBag
     */
    private $headerBag;

    /**
     * SymfonyHeaderBagAdapter constructor.
     * @param \Symfony\Component\HttpFoundation\HeaderBag $headerBag
     */
    public function __construct(\Symfony\Component\HttpFoundation\HeaderBag $headerBag)
    {
        $this->headerBag = $headerBag;
    }

    /**
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    function get(string $key, $default = null)
    {
        return $this->headerBag->get($key, $default);
    }

    /**
     * @param string $key
     * @param $value
     */
    function set(string $key, $value)
    {
        return $this->headerBag->set($key, $value);
    }

    /**
     * @param string $key
     * @return bool
     */
    function has(string $key): bool
    {
        return $this->headerBag->has($key);
    }
}