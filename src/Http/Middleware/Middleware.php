<?php

namespace Strictly\Http\Middleware;

interface Middleware
{
    /**
     * Execute middleware.
     */
    function handle();
}