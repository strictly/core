<?php

namespace Strictly\Http\Middleware;

interface MiddlewareGroup
{
    /**
     * @return Middleware[]
     */
    function middlewares(): array;
}