<?php

namespace Strictly\Http\Response;

use Symfony\Component\HttpFoundation\Response;

class SymfonyAdapter implements ResponseInterface
{
    /**
     * @var Response
     */
    private $symfonyResponse;

    /**
     * SymfonyAdapter constructor.
     * @param Response $symfonyResponse
     */
    public function __construct(Response $symfonyResponse)
    {
        $this->symfonyResponse = $symfonyResponse;
    }

    /**
     * Sends the response.
     */
    function send()
    {
        $this->symfonyResponse->send();
    }
}