<?php

namespace Strictly\Http\Response;

interface ResponseFactory
{
    /**
     * @param string $content
     * @param int $status
     * @return ResponseInterface
     */
    function make(string $content, int $status = 200): ResponseInterface;

    /**
     * @param array $data
     * @param int $status
     * @return ResponseInterface
     */
    function makeJson(array $data, int $status = 200): ResponseInterface;
}