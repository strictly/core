<?php

namespace Strictly\Http\Response;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class SymfonyAdapterFactory implements ResponseFactory
{
    /**
     * @param string $content
     * @param int $status
     * @return ResponseInterface
     */
    function make(string $content, int $status = 200): ResponseInterface
    {
        return new SymfonyAdapter(
            new Response($content, $status)
        );
    }

    /**
     * @param array $data
     * @param int $status
     * @return ResponseInterface
     */
    function makeJson(array $data, int $status = 200): ResponseInterface
    {
        return new SymfonyAdapter(
            new JsonResponse($data, $status)
        );
    }
}