<?php

namespace Strictly\Http\Response;

interface ResponseInterface
{
    /**
     * Sends the response.
     */
    function send();
}