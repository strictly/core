<?php

namespace Strictly\Http;

interface HeaderBag
{
    /**
     * @param string $key
     * @param mixed $default
     * @return mixed
     * @return mixed
     */
    function get(string $key, $default = null);

    /**
     * @param string $key
     * @param $value
     */
    function set(string $key, $value);

    /**
     * @param string $key
     * @return bool
     */
    function has(string $key): bool;
}