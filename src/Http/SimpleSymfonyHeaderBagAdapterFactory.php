<?php

namespace Strictly\Http;

use Symfony\Component\HttpFoundation\HeaderBag;

class SimpleSymfonyHeaderBagAdapterFactory
{
    /**
     * @param HeaderBag $headerBag
     * @return SymfonyHeaderBagAdapter
     */
    function make(HeaderBag $headerBag): SymfonyHeaderBagAdapter {
        return new SymfonyHeaderBagAdapter($headerBag);
    }
}