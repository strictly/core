<?php

namespace Strictly\Http;

use Strictly\Http\Response\ResponseInterface;
use Throwable;

interface ExceptionHandler
{
    /**
     * @param Throwable $throwable
     * @return ResponseInterface
     */
    function handle(Throwable $throwable): ResponseInterface;
}