<?php

namespace Strictly\Http\Routing;

use Strictly\Container\Container;
use Strictly\Http\Exceptions\HttpNotFoundException;
use Strictly\Http\Request\RequestInterface;
use Strictly\Http\Routing\Route\Repository;
use Strictly\Http\Routing\Route\Route;

class RouteRouteResolver implements RouteResolverInterface
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var Repository
     */
    private $repository;

    /**
     * @var Container
     */
    private $container;

    /**
     * Resolver constructor.
     * @param RequestInterface $request
     * @param Repository $repository
     * @param Container $container
     */
    public function __construct(RequestInterface $request, Repository $repository, Container $container)
    {
        $this->request = $request;
        $this->repository = $repository;
        $this->container = $container;
    }

    /**
     * @return Route
     * @throws \Strictly\Http\Exceptions\HttpNotFoundException
     */
    function resolve(): Route {
        if(!$route = $this->repository->get($this->request->getMethod(), $this->request->getPath())) {
            throw new HttpNotFoundException($this->request, "Not found.", 404);
        }

        return $route;
    }
}