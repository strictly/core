<?php

namespace Strictly\Http\Routing;

use Strictly\Http\Response\ResponseInterface;

interface RequestHandlerInterface
{
    /**
     * @return ResponseInterface
     */
    function handle(): ResponseInterface;
}