<?php

namespace Strictly\Http\Routing\Route;

class Route
{
    /**
     * @var string
     */
    private $method;

    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $requestHandler;

    /**
     * @var string[]
     */
    private $middlewares = [];

    /**
     * Route constructor.
     * @param string $method
     * @param string $path
     * @param string $requestHandler
     */
    function __construct(string $method, string $path, string $requestHandler)
    {
        $this->method = $method;
        $this->path = $path;
        $this->requestHandler = $requestHandler;
    }

    /**
     * @param string $middleware
     * @return Route
     */
    function addMiddleware(string $middleware): Route {
        $this->middlewares[] = $middleware;

        return $this;
    }

    /**
     * @return string[]
     */
    function getMiddlewares(): array {
        return $this->middlewares;
    }

    /**
     * @return string
     */
    function getRequestHandler(): string {
        return $this->requestHandler;
    }
}