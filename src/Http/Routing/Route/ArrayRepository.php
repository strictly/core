<?php

namespace Strictly\Http\Routing\Route;

use Adbar\Dot;

class ArrayRepository implements Repository
{
    /**
     * @var Dot
     */
    private $routes;

    /**
     * @var RouteFactory
     */
    private $routeFactory;

    /**
     * ArrayRepository constructor.
     * @param Dot $dot
     * @param RouteFactory $routeFactory
     */
    public function __construct(Dot $dot, RouteFactory $routeFactory)
    {
        $this->routes = $dot;
        $this->routeFactory = $routeFactory;
    }

    /**
     * @param string $method
     * @param string $path
     * @param string $requestHandler
     * @return Route
     */
    function set(string $method, string $path, string $requestHandler): Route
    {
        $route = $this->routeFactory->makeRoute($method, $path, $requestHandler);
        $this->routes->set("{$method}.{$path}", $route);

        return $route;
    }

    /**
     * @param string $method
     * @param string $path
     * @return Route|null
     */
    function get(string $method, string $path)
    {
        return $this->routes->get("{$method}.{$path}");
    }
}