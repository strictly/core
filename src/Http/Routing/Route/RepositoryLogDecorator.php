<?php

namespace Strictly\Http\Routing\Route;

use Strictly\Log\Writer;

class RepositoryLogDecorator implements Repository
{
    /**
     * @var Repository
     */
    private $repository;

    /**
     * @var Writer
     */
    private $writer;

    /**
     * RepositoryLogDecorator constructor.
     * @param Repository $repository
     * @param Writer $writer
     */
    public function __construct(Repository $repository, Writer $writer)
    {
        $this->repository = $repository;
        $this->writer = $writer;
    }

    /**
     * @param string $method
     * @param string $path
     * @param string $requestHandler
     * @return Route
     */
    function set(string $method, string $path, string $requestHandler): Route
    {
        $this->writer->debug("Set Request Handler ({$method},{$path}): {$requestHandler}");
        return $this->repository->set($method, $path, $requestHandler);
    }

    /**
     * @param string $method
     * @param string $path
     * @return Route|null
     */
    function get(string $method, string $path)
    {
        $resolverReference = $this->repository->get($method, $path);
        $this->writer->debug("Get Request Handler ({$method},{$path}): ".($resolverReference ?: "Not Found"));
        return $resolverReference;
    }
}