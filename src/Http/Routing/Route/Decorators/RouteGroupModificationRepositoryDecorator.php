<?php

namespace Strictly\Http\Routing\Route\Decorators;

use Strictly\Http\Routing\Route\Repository;
use Strictly\Http\Routing\Route\Route;

class RouteGroupModificationRepositoryDecorator implements Repository
{
    /**
     * @var Repository
     */
    private $repository;

    /**
     * @var array
     */
    private $middlewares;

    /**
     * ApplyMiddlewareRepositoryDecorator constructor.
     * @param Repository $repository
     * @param array $middlewares
     */
    public function __construct(Repository $repository, array $middlewares)
    {
        $this->repository = $repository;
        $this->middlewares = $middlewares;
    }

    /**
     * @param string $method
     * @param string $path
     * @param string $requestHandler
     * @return Route
     */
    function set(string $method, string $path, string $requestHandler): Route
    {
        $route = $this->repository->set($method, $path, $requestHandler);

        foreach($this->middlewares as $middleware) {
            $route->addMiddleware($middleware);
        }

        return $route;
    }

    /**
     * @param string $method
     * @param string $path
     * @return Route|null
     */
    function get(string $method, string $path)
    {
        return $this->repository->get($method, $path);
    }
}