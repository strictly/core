<?php

namespace Strictly\Http\Routing\Route;

interface Repository
{
    /**
     * @param string $method
     * @param string $path
     * @param string $requestHandler
     * @return Route
     */
    function set(string $method, string $path, string $requestHandler): Route;

    /**
     * @param string $method
     * @param string $path
     * @return Route|null
     */
    function get(string $method, string $path);
}