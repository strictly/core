<?php

namespace Strictly\Http\Routing\Route;

class RouteFactory
{
    /**
     * @param string $method
     * @param string $path
     * @param string $requestHandler
     * @return Route
     */
    function makeRoute(string $method, string $path, string $requestHandler): Route {
        return new Route($method, $path, $requestHandler);
    }
}