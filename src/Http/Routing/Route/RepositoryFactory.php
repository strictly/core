<?php

namespace Strictly\Http\Routing\Route;

use Strictly\Container\ResolverInterface;
use Strictly\Foundation\Contracts\Factory;
use Strictly\Http\Routing\Route\RepositoryLogDecorator;
use Strictly\Http\Routing\RouteResolverInterface;
use Strictly\Log\Writer;
use Strictly\Configuration\Repository as ConfigurationRepository;

class RepositoryFactory implements Factory
{
    /**
     * @var RouteResolverInterface
     */
    private $resolver;

    /**
     * @var ConfigurationRepository
     */
    private $configuration;

    /**
     * RepositoryFactory constructor.
     * @param ResolverInterface $resolver
     * @param ConfigurationRepository $configuration
     */
    public function __construct(ResolverInterface $resolver, ConfigurationRepository $configuration)
    {
        $this->resolver = $resolver;
        $this->configuration = $configuration;
    }

    /**
     * @return mixed
     * @throws \Strictly\Http\Exceptions\HttpNotFoundException
     */
    function make(): Repository
    {
        $repository = $this->resolver->resolve(ArrayRepository::class);

        if($this->configuration->get('routing.log.repository')) {
            $repository = new RepositoryLogDecorator(
                $repository,
                $this->resolver->resolve(Writer::class)
            );
        }

        return $repository;
    }
}