<?php

namespace Strictly\Http\Routing;

use Strictly\Container\Container;
use Strictly\Http\Routing\Route\Route;

class PipelineFactory
{
    /**
     * @var Container
     */
    private $container;

    /**
     * SimplePipelineFactory constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @param Route $route
     * @return Pipeline
     */
    function makePipeline(Route $route): Pipeline {
        $pipeline = new Pipeline($this->container->resolve($route->getRequestHandler()));

        foreach($route->getMiddlewares() as $middleware) {
            $pipeline->addMiddleware(
                $this->container->resolve($middleware)
            );
        }

        return $pipeline;
    }
}