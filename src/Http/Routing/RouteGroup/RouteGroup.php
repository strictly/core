<?php

namespace Strictly\Http\Routing\RouteGroup;

use Closure;

class RouteGroup
{
    /**
     * @var Closure
     */
    private $routes;

    /**
     * @var string[]
     */
    private $middlewares = [];

    /**
     * RouteGroup constructor.
     * @param Closure $routes
     */
    public function __construct(Closure $routes)
    {
        $this->routes = $routes;
    }

    /**
     * @return Closure
     */
    function getRouteClosure(): Closure {
        return $this->routes;
    }

    /**
     * @param string $middleware
     * @return RouteGroup
     */
    function addMiddleware(string $middleware): RouteGroup {
        $this->middlewares[] = $middleware;

        return $this;
    }

    /**
     * @param array $middlewares
     * @return RouteGroup
     */
    function addMiddlewares(array $middlewares): RouteGroup {
        $this->middlewares = array_merge($this->middlewares, $middlewares);

        return $this;
    }

    /**
     * @return array
     */
    function getMiddlewares(): array {
        return $this->middlewares;
    }
}