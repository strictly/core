<?php

namespace Strictly\Http\Routing\RouteGroup;

use Closure;

interface Repository
{
    /**
     * @param Closure $routes
     * @return RouteGroup
     */
    function addRouteGroup(Closure $routes): RouteGroup;

    /**
     * @return RouteGroup[]
     */
    function all();
}