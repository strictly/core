<?php

namespace Strictly\Http\Routing\RouteGroup;

use Closure;

class ArrayRepository implements Repository
{
    /**
     * @var SimpleRouteGroupFactory
     */
    private $routeGroupFactory;

    /**
     * ArrayRepository constructor.
     * @param SimpleRouteGroupFactory $routeGroupFactory
     */
    public function __construct(SimpleRouteGroupFactory $routeGroupFactory)
    {
        $this->routeGroupFactory = $routeGroupFactory;
    }

    /**
     * @var RouteGroup[]
     */
    private $routesGroups = [];

    /**
     * @param Closure $routes
     * @return RouteGroup
     */
    function addRouteGroup(Closure $routes): RouteGroup
    {
        $routeGroup = $this->routeGroupFactory->makeRouteGroup($routes);
        $this->routesGroups[] = $routeGroup;
        return $routeGroup;
    }

    /**
     * @return RouteGroup[]
     */
    function all()
    {
        return $this->routesGroups;
    }
}