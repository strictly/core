<?php

namespace Strictly\Http\Routing\RouteGroup;

use Strictly\Container\Container;
use Strictly\Http\Routing\Manager;
use Strictly\Http\Routing\Route\Decorators\RouteGroupModificationRepositoryDecorator;
use Strictly\Http\Routing\Route\Repository as RouteRepository;

class RouteGroupProcessor
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var SimpleRepositoryFactory
     */
    private $routeGroupFactory;

    /**
     * RouteGroupProcessor constructor.
     * @param Container $container
     * @param SimpleRepositoryFactory $routeGroupFactory
     */
    public function __construct(Container $container, SimpleRepositoryFactory $routeGroupFactory)
    {
        $this->container = $container;
        $this->routeGroupFactory = $routeGroupFactory;
    }

    /**
     * @param RouteGroup[] $routeGroups
     * @param RouteRepository $routeRepository
     */
    function registerRouteGroups(array $routeGroups, RouteRepository $routeRepository) {
        foreach($routeGroups as $routeGroup) {
            $this->registerRouteGroup($routeGroup, $routeRepository);
        }
    }

    /**
     * @param RouteGroup $routeGroup
     * @param RouteRepository $routeRepository
     */
    private function registerRouteGroup(RouteGroup $routeGroup, RouteRepository $routeRepository) {
        if($routeGroup->getMiddlewares()) {
            $routeRepository = new RouteGroupModificationRepositoryDecorator($routeRepository, $routeGroup->getMiddlewares());
        }

        $nextLevelRouteGroupRepository = $this->routeGroupFactory->makeRepository();

        $tmpRouter = new Manager(
            $routeRepository,
            $nextLevelRouteGroupRepository
        );
        $routeGroup->getRouteClosure()($tmpRouter);

        $this->registerRouteGroups($nextLevelRouteGroupRepository->all(), $routeRepository);
    }
}