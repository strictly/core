<?php

namespace Strictly\Http\Routing\RouteGroup;

class SimpleRepositoryFactory
{
    /**
     * @var SimpleRouteGroupFactory
     */
    private $routeGroupFactory;

    /**
     * SimpleRepositoryFactory constructor.
     * @param SimpleRouteGroupFactory $routeGroupFactory
     */
    public function __construct(SimpleRouteGroupFactory $routeGroupFactory)
    {
        $this->routeGroupFactory = $routeGroupFactory;
    }

    /**
     * @return Repository
     */
    function makeRepository(): Repository {
        return new ArrayRepository(
            $this->routeGroupFactory
        );
    }
}