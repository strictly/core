<?php

namespace Strictly\Http\Routing\RouteGroup;

use Closure;

class SimpleRouteGroupFactory
{
    /**
     * @param Closure $routes
     * @return RouteGroup
     */
    function makeRouteGroup(Closure $routes): RouteGroup {
        return new RouteGroup($routes);
    }
}