<?php

namespace Strictly\Http\Routing;

use Closure;
use Strictly\Http\Routing\Route\Repository;
use Strictly\Http\Routing\Route\Route;
use Strictly\Http\Routing\RouteGroup\RouteGroup;
use Strictly\Http\Routing\RouteGroup\Repository as RouteGroupRepository;

class Manager implements ManagerInterface
{
    /**
     * @var Repository
     */
    private $routes;

    /**
     * @var RouteGroupRepository
     */
    private $routeGroups;

    /**
     * Manager constructor.
     * @param Repository $routes
     * @param RouteGroupRepository $routeGroups
     */
    public function __construct(Repository $routes, RouteGroupRepository $routeGroups)
    {
        $this->routes = $routes;
        $this->routeGroups = $routeGroups;
    }

    /**
     * @param string $path
     * @param string $requestHandler
     * @return Route
     */
    function get(string $path, string $requestHandler): Route {
        return $this->routes->set('GET', $path, $requestHandler);
    }

    /**
     * @param string $path
     * @param string $requestHandler
     * @return Route
     */
    function post(string $path, string $requestHandler): Route {
        return $this->routes->set('POST', $path, $requestHandler);
    }

    /**
     * @param string $path
     * @param string $requestHandler
     * @return Route
     */
    function put(string $path, string $requestHandler): Route {
        return $this->routes->set('PUT', $path, $requestHandler);
    }

    /**
     * @param string $path
     * @param string $requestHandler
     * @return Route
     */
    function update(string $path, string $requestHandler): Route {
        return $this->routes->set('UPDATE', $path, $requestHandler);
    }

    /**
     * @param string $path
     * @param string $requestHandler
     * @return Route
     */
    function delete(string $path, string $requestHandler): Route {
        return $this->routes->set('DELETE', $path, $requestHandler);
    }

    /**
     * @param Closure $routes
     * @return RouteGroup
     */
    function group(Closure $routes): RouteGroup
    {
        return $this->routeGroups->addRouteGroup($routes);
    }
}