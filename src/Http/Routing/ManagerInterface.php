<?php

namespace Strictly\Http\Routing;

use Closure;
use Strictly\Http\Routing\Route\Route;
use Strictly\Http\Routing\RouteGroup\RouteGroup;

interface ManagerInterface
{
    /**
     * @param string $path
     * @param string $requestHandler
     * @return Route
     */
    function get(string $path, string $requestHandler): Route;

    /**
     * @param string $path
     * @param string $requestHandler
     * @return Route
     */
    function post(string $path, string $requestHandler): Route;

    /**
     * @param string $path
     * @param string $requestHandler
     * @return Route
     */
    function put(string $path, string $requestHandler): Route;

    /**
     * @param string $path
     * @param string $requestHandler
     * @return Route
     */
    function update(string $path, string $requestHandler): Route;

    /**
     * @param string $path
     * @param string $requestHandler
     * @return Route
     */
    function delete(string $path, string $requestHandler): Route;

    /**
     * @param Closure $routes
     * @return RouteGroup
     */
    function group(Closure $routes): RouteGroup;
}