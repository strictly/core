<?php

namespace Strictly\Http\Routing;

use Strictly\Http\Middleware\Middleware;
use Strictly\Http\Response\ResponseInterface;

class Pipeline
{
    /**
     * @var RequestHandlerInterface
     */
    private $requestHandler;

    /**
     * @var Middleware[]
     */
    private $middlwares = [];

    /**
     * Pipeline constructor.
     * @param RequestHandlerInterface $requestHandler
     */
    public function __construct(RequestHandlerInterface $requestHandler)
    {
        $this->requestHandler = $requestHandler;
    }

    /**
     * @param Middleware $middleware
     * @return Pipeline
     */
    function addMiddleware(Middleware $middleware): Pipeline {
        $this->middlwares[] = $middleware;

        return $this;
    }

    /**
     * @return ResponseInterface
     */
    function handle(): ResponseInterface
    {
        foreach($this->middlwares as $middleware) {
            $middleware->handle();
        }

        return $this->requestHandler->handle();
    }
}