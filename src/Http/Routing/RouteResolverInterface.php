<?php

namespace Strictly\Http\Routing;

use Strictly\Http\Exceptions\HttpNotFoundException;
use Strictly\Http\Routing\Route\Route;

interface RouteResolverInterface
{
    /**
     * @return Route
     * @throws HttpNotFoundException
     */
    function resolve(): Route;
}