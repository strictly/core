<?php

namespace Strictly\Http\Exceptions;

use Exception;
use Strictly\Http\Request\RequestInterface;
use Throwable;

abstract class HttpException extends Exception
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * HttpException constructor.
     * @param RequestInterface $request
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(RequestInterface $request, string $message = "", int $code = 0, Throwable $previous = null)
    {
        $this->request = $request;

        parent::__construct($message, $code, $previous);
    }
}