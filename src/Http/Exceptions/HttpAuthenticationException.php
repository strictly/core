<?php

namespace Strictly\Http\Exceptions;

use Strictly\Http\Request\RequestInterface;
use Throwable;

class HttpAuthenticationException extends HttpException {
    public function __construct(RequestInterface $request, string $message = "Unauthenticated.", int $code = 401, Throwable $previous = null)
    {
        parent::__construct($request, $message, $code, $previous);
    }
}