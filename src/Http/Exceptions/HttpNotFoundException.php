<?php

namespace Strictly\Http\Exceptions;

use Strictly\Http\Request\RequestInterface;
use Throwable;

class HttpNotFoundException extends HttpException {
    public function __construct(RequestInterface $request, string $message = "", int $code = 404, Throwable $previous = null)
    {
        parent::__construct($request, $message, $code, $previous);
    }
}