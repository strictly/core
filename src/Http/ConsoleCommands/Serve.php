<?php

namespace Strictly\Http\ConsoleCommands;

use Strictly\Console\ConsoleCommand;
use Strictly\Filesystem\Repository;

class Serve implements ConsoleCommand
{
    /**
     * @var Repository
     */
    private $paths;

    /**
     * Serve constructor.
     * @param Repository $paths
     */
    public function __construct(Repository $paths)
    {
        $this->paths = $paths;
    }

    /**
     * Execute the command.
     */
    function execute()
    {
        print 'Serving the application on 127.0.0.1:8000'.PHP_EOL;
        exec("php -S 127.0.0.1:8000 -t {$this->paths->getPublicPath()}");
    }

    /**
     * @return string
     */
    function getName(): string
    {
        return 'serve';
    }

    /**
     * @return string
     */
    function getDescription(): string
    {
        return 'Serve the application locally.';
    }
}