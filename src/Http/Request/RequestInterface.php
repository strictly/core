<?php

namespace Strictly\Http\Request;

use Strictly\Http\HeaderBag;

interface RequestInterface
{
    /**
     * @return ParameterBagInterface
     */
    function query(): ParameterBagInterface;

    /**
     * @return ParameterBagInterface
     */
    function request(): ParameterBagInterface;

    /**
     * @return HeaderBag
     */
    function headers(): HeaderBag;

    /**
     * @return string
     */
    function getUri(): string;

    /**
     * @return string
     */
    function getPath(): string;

    /**
     * @return string
     */
    function getPathAndQuery(): string;

    /**
     * @return string
     */
    function getHost(): string;

    /**
     * @return string
     */
    function getScheme(): string;

    /**
     * @return string
     */
    function getMethod(): string;
}