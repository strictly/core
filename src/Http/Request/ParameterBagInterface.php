<?php

namespace Strictly\Http\Request;

interface ParameterBagInterface
{
    /**
     * @param string $key
     * @param mixed $value
     */
    function set(string $key, $value);

    /**
     * @param string $key
     * @return bool
     */
    function has(string $key): bool;

    /**
     * @param string $key
     * @param null $default
     * @return mixed
     */
    function get(string $key, $default = null);

    /**
     * @param string $key
     * @param bool $default
     * @return bool
     */
    function getBool(string $key, $default = false): bool;

    /**
     * @param string $key
     * @param string $default
     * @return string
     */
    function getString(string $key, $default = ''): string;

    /**
     * @param string $key
     * @param int $default
     * @return int
     */
    function getInt(string $key, $default = 0): int;

    /**
     * @return array
     */
    function all();
}