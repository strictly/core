<?php

namespace Strictly\Http\Request;

use Symfony\Component\HttpFoundation\ParameterBag;

class SymfonyParameterBagAdapter implements ParameterBagInterface
{
    /**
     * @var ParameterBag
     */
    private $parameterBag;

    /**
     * MessageBag constructor.
     * @param ParameterBag $parameterBag
     */
    function __construct(ParameterBag $parameterBag)
    {
        $this->parameterBag = $parameterBag;
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    function set(string $key, $value)
    {
        $this->parameterBag->set($key, $value);
    }

    /**
     * @param string $key
     * @return bool
     */
    function has(string $key): bool
    {
        return $this->parameterBag->has($key);
    }

    /**
     * @param string $key
     * @param null $default
     * @return mixed
     */
    function get(string $key, $default = null)
    {
        return $this->parameterBag->get($key, $default);
    }

    /**
     * @param string $key
     * @param bool $default
     * @return bool
     */
    function getBool(string $key, $default = false): bool
    {
        return $this->parameterBag->getBoolean($key, $default);
    }

    /**
     * @param string $key
     * @param string $default
     * @return string
     */
    function getString(string $key, $default = ''): string
    {
        return strval($this->parameterBag->get($key, $default));
    }

    /**
     * @param string $key
     * @param int $default
     * @return int
     */
    function getInt(string $key, $default = 0): int
    {
        return $this->parameterBag->getInt($key, $default);
    }

    /**
     * @return array
     */
    function all()
    {
        return $this->parameterBag->all();
    }
}