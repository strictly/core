<?php

namespace Strictly\Http\Request;

use Strictly\Http\HeaderBag;
use Strictly\Http\SimpleSymfonyHeaderBagAdapterFactory;
use Symfony\Component\HttpFoundation\Request;

class SymfonyAdapter implements RequestInterface
{
    /**
     * @var Request
     */
    private $symfonyRequest;

    /**
     * @var SimpleSymfonyParameterBagAdapterFactory
     */
    private $parameterBagAdapterFactory;

    /**
     * @var SimpleSymfonyHeaderBagAdapterFactory
     */
    private $headerBagAdapterFactory;

    /**
     * SymfonyAdapter constructor.
     * @param Request $symfonyRequest
     * @param SimpleSymfonyParameterBagAdapterFactory $parameterBagAdapterFactory
     * @param SimpleSymfonyHeaderBagAdapterFactory $headerBagAdapterFactory
     */
    public function __construct(Request $symfonyRequest, SimpleSymfonyParameterBagAdapterFactory $parameterBagAdapterFactory, SimpleSymfonyHeaderBagAdapterFactory $headerBagAdapterFactory)
    {
        $this->symfonyRequest = $symfonyRequest;
        $this->parameterBagAdapterFactory = $parameterBagAdapterFactory;
        $this->headerBagAdapterFactory = $headerBagAdapterFactory;
    }

    /**
     * @return ParameterBagInterface
     */
    function query(): ParameterBagInterface
    {
        return $this->parameterBagAdapterFactory->make($this->symfonyRequest->query);
    }

    /**
     * @return ParameterBagInterface
     */
    function request(): ParameterBagInterface
    {
        return $this->parameterBagAdapterFactory->make($this->symfonyRequest->request);
    }

    /**
     * @return HeaderBag
     */
    function headers(): HeaderBag
    {
        return $this->headerBagAdapterFactory->make($this->symfonyRequest->headers);
    }

    /**
     * @return string
     */
    function getUri(): string
    {
        return $this->symfonyRequest->getUri();
    }

    /**
     * @return string
     */
    function getPath(): string
    {
        return $this->symfonyRequest->getPathInfo();
    }

    /**
     * @return string
     */
    function getHost(): string
    {
        return $this->symfonyRequest->getHost();
    }

    /**
     * @return string
     */
    function getPathAndQuery(): string
    {
        return $this->symfonyRequest->getRequestUri();
    }

    /**
     * @return string
     */
    function getScheme(): string
    {
        return $this->symfonyRequest->getScheme();
    }

    /**
     * @return string
     */
    function getMethod(): string
    {
        return $this->symfonyRequest->getMethod();
    }
}