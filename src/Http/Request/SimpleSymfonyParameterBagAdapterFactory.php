<?php

namespace Strictly\Http\Request;

use Symfony\Component\HttpFoundation\ParameterBag;

class SimpleSymfonyParameterBagAdapterFactory
{
    /**
     * @param ParameterBag $parameterBag
     * @return ParameterBagInterface
     */
    function make(ParameterBag $parameterBag): ParameterBagInterface {
        return new SymfonyParameterBagAdapter($parameterBag);
    }
}