<?php

namespace Strictly\Http\Request;

use Strictly\Container\Container;
use Strictly\Foundation\Contracts\Factory;
use Strictly\Http\SimpleSymfonyHeaderBagAdapterFactory;
use Strictly\Http\SymfonyHeaderBagAdapter;
use Symfony\Component\HttpFoundation\Request;

class SymfonyAdapterFactory implements Factory
{
    /**
     * @var Container
     */
    private $container;

    /**
     * SymfonyAdapterFactory constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @return SymfonyAdapter
     */
    function make(): SymfonyAdapter
    {
        return new SymfonyAdapter(
            Request::createFromGlobals(),
            $this->container->resolve(SimpleSymfonyParameterBagAdapterFactory::class),
            $this->container->resolve(SimpleSymfonyHeaderBagAdapterFactory::class)
        );
    }
}