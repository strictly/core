<?php

namespace Strictly\Http;

use Strictly\Container\Container;
use Strictly\Foundation\Contracts\KernelInterface;
use Strictly\Http\Response\ResponseInterface;
use Strictly\Http\Routing\PipelineFactory;
use Strictly\Http\Routing\RouteResolverInterface;
use Throwable;

class Application implements ApplicationInterface
{
    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @var Container
     */
    private $container;

    /**
     * Application constructor.
     * @param KernelInterface $kernel
     * @param Container $container
     */
    public function __construct(
        KernelInterface $kernel,
        Container $container
    ) {
        $this->kernel = $kernel;
        $this->container = $container;
    }

    /**
     * @return ResponseInterface
     */
    function handle(): ResponseInterface
    {
        $this->kernel->boot();

        /** @var ExceptionHandler $exceptionHandler */
        $exceptionHandler = $this->container->resolve(ExceptionHandler::class);

        try {
            /** @var PipelineFactory $pipelineFactory */
            $pipelineFactory= $this->container->resolve(PipelineFactory::class);

            /** @var RouteResolverInterface $routeResolver */
            $routeResolver = $this->container->resolve(RouteResolverInterface::class);

            $pipeline = $pipelineFactory->makePipeline($routeResolver->resolve());

            $response = $pipeline->handle();
        } catch (Throwable $throwable) {
            $response = $exceptionHandler->handle($throwable);
        }
        $this->kernel->terminate();
        return $response;
    }
}